const winston = require("winston");
const mongoose = require("mongoose");
const config = require("config");

module.exports = function () {
  const database = config.get("db");
  const username = config.get("dbuser");
  const password = config.get("dbpassword");

  const options = {
    useNewUrlParser: true,
    user: username,
    pass: password,
  };
  //mongoose.Promise = global.Promise;
  mongoose
    .connect(database)
    .then(() => {
      console.log(`Connected to ${database}...`);
      winston.info(`Connected to ${database}...`);
    })
    .catch((err) => {
      console.log(err);
      winston.info(`error connecting to the database on ${database}`);
      process.exit();
    });
};
