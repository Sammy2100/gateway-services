const winston = require("winston");
require("express-async-errors");

const logBaseFolder = "logs/";

module.exports = () => {
  winston.exceptions.handle(
    new winston.transports.Console({ colorize: true, prettyPrint: true }),
    new winston.transports.File({
      filename: logBaseFolder + "uncaughtExceptions.log",
    })
  );

  process.on("unhandledRejection", (ex) => {
    throw ex;
  });

  winston.add(
    new winston.transports.File({ filename: logBaseFolder + "logfile.log" })
  );
};
