const express = require("express");

const deviceController = require("../routes/device/controllers/device.controller");
const gatewayController = require("../routes/gateway/controllers/gateway.controller");

module.exports = (app) => {
  app.use(express.json());
  app.use("/api/device", deviceController);
  app.use("/api/gateway", gatewayController);
};
