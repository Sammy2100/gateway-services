const serviceResponse = {
  message: "",
  result: {},
  successful: false,
};

module.exports = {
  serviceResponse,
};
