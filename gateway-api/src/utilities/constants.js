var config = require("config");

const CONSTANTS = {
  deviceLimitPerGateway: config.deviceLimitPerGateway,
};

module.exports = {
  CONSTANTS,
};
