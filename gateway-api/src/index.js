var express = require("express");
var config = require("config");

var app = express();
require("./startup/logging")();
require("./startup/cors")(app);
require("./startup/routes")(app);
require("./startup/db")();

var port = config.nodeport;
var server = app.listen(port, function () {
  console.log(`listening to port... ${port}`);
});

module.exports = server;
