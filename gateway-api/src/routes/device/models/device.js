const { Validator } = require("node-input-validator");
const mongoose = require("mongoose");

const Device = mongoose.model(
  "Device",
  new mongoose.Schema({
    uid: {
      type: Number,
      required: true,
    },
    gatewayId: {
      type: String,
      required: true,
    },
    vendor: {
      type: String,
      required: true,
      minlength: 5,
    },
    status: {
      type: Boolean,
      default: false,
    },
    dateCreated: {
      type: Date,
    },
  })
);

const validateDevice = async (device) => {
  const schema = {
    uid: "required",
    vendor: "required",
    gatewayId: "required",
    status: "required",
  };
  const customMessages = {
    uid: "uid is required",
    vendor: "vendor is required",
    gatewayId: "Gateway is required",
  };
  const v = new Validator(device, schema, customMessages);
  const isValid = await v.check();
  return { isValid, errorMessages: v.customMessages };
};

module.exports = {
  Device,
  validate: validateDevice,
};
