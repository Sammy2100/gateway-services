const express = require("express");

const router = express.Router();
const validateObjectId = require("../../../middleware/validateObjectId");
const {
  getAll,
  add,
  update,
  remove,
  getById,
  getByGatewayId,
} = require("../services/device.service");

router.get("/", async (req, res) => {
  const response = await getAll();

  if (response.successful) {
    res.send(response);
  } else {
    return res.status(400).send(response);
  }
});

router.post("/", async (req, res) => {
  try {
    const response = await add(req.body);

    res.send(response);
  } catch (ex) {
    console.log(ex);
    return res
      .status(400)
      .send(
        "Ann error occured while trying to link this devie. Try again later."
      );
  }
});

router.put("/:id", async (req, res) => {
  const response = await update(req.body, req.params.id);

  if (response.successful) {
    res.send(response);
  } else {
    return res.status(400).send(response);
  }
});

router.delete("/:id", validateObjectId, async (req, res) => {
  const response = await remove(req.params.id);

  if (response.successful) {
    res.send(response);
  } else {
    return res.status(400).send(response);
  }
});

router.get("/:id", validateObjectId, async (req, res) => {
  const response = await getById(req.params.id);

  if (response.successful) {
    res.send(response);
  } else {
    return res.status(400).send(response);
  }
});

router.get("/getByGatewayId/:id", validateObjectId, async (req, res) => {
  const response = await getByGatewayId(req.params.id);

  if (response.successful) {
    res.send(response);
  } else {
    return res.status(400).send(response);
  }
});

module.exports = router;
