const { CONSTANTS } = require("../../../utilities/constants");
const { serviceResponse } = require("../../../utilities/serviceResponse");
const device = require("../models/device");
const { Device, validate } = require("../models/device");

const getAll = async () => {
  // get all devices
  const devices = await Device.find().select("-__v").sort("vendor");

  console.log(devices);
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = devices;

  return serviceResponse;
};

const getById = async (deviceId) => {
  // check if the device exist
  const device = await Device.findById(deviceId).select("-__v");

  // return failure response if device does not exist
  if (!device) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "The device with the given ID does not exist";
    return serviceResponse;
  }

  // returns success response if device exist
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = device;

  return serviceResponse;
};

const add = async (model) => {
  // validate user input
  const modelState = await validate(model);
  if (!modelState.isValid) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = modelState.errorMessages;
    return serviceResponse;
  }

  // check if the uid is unique
  const serialNo = await Device.findOne({ uid: model.uid });
  if (serialNo) {
    serviceResponse.message = `A device with uid [${model.uid}] already exist.`;
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // check for device limit per gateway
  const devicesResponse = await getByGatewayId(model.gatewayId);
  if (
    devicesResponse.successful &&
    devicesResponse.result.length >= CONSTANTS.deviceLimitPerGateway
  ) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = `Device limit [${CONSTANTS.deviceLimitPerGateway}] reached for the selected gateway.`;
    return serviceResponse;
  }

  // prepare the device model
  let device = new Device({
    uid: model.uid,
    gatewayId: model.gatewayId,
    vendor: model.vendor,
    status: model.status,
    dateCreated: new Date(),
  });

  // add the device to database
  device = await device.save();

  // return failure response if add operations fails
  if (!device) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "Unable to save device information";
    return serviceResponse;
  }

  // return success response when add operation was successful
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = device;

  return serviceResponse;
};

const update = async (model, deviceId) => {
  //validate the dvice id
  if (model.id != deviceId) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "Invalid device. Bad request";
    return serviceResponse;
  }

  // validate the users input
  const modelState = await validate(model);
  if (!modelState.isValid) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = modelState.errorMessages;
    return serviceResponse;
  }

  // find and update the device
  const device = await Device.findByIdAndUpdate(
    deviceId,
    {
      uid: model.uid,
      gatewayId: model.gatewayId,
      vendor: model.vendor,
      status: model.status,
      dateCreated: new Date(),
    },
    { new: true }
  );

  // return failure response if update operations fails
  if (!device) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "The device with the given ID does not exist";
    return serviceResponse;
  }

  // return success response if update operations fails
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = device;

  return serviceResponse;
};

const remove = async (deviceId) => {
  // retrieve and remove existing device by the device id
  const device = await Device.findByIdAndRemove(deviceId);

  // return failure response if devele operations fails
  if (!device) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "The device with the given ID does not exist";
    return serviceResponse;
  }

  // return success response if delete operation is successful
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = device;

  return serviceResponse;
};

const getByGatewayId = async (gatewayId) => {
  // get a list of devices by the gateway id
  const devices = await Device.find({ gatewayId: gatewayId });

  // validate and return failure response if no device was found
  if (device.length == 0) {
    serviceResponse.successful = false;
    serviceResponse.result = [];
    serviceResponse.message = "The device with the given ID does not exist";
    return serviceResponse;
  }

  // return a success response when devices device were found
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = devices;

  return serviceResponse;
};

module.exports = {
  getAll,
  getById,
  getByGatewayId,
  add,
  update,
  remove,
};
