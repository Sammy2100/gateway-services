const { serviceResponse } = require("../../../utilities/serviceResponse");
const { Device } = require("../../device/models/device");
const { Gateway, validate } = require("../models/gateway");

const getAll = async () => {
  // get all gateway
  const gateways = await Gateway.find().select("-__v").sort("name");
  // retrieve all the gateways' ids to retrieve associated devices
  const gatewayIds = gateways.map((g) => g._id.toString());
  // get the list of devices to retrieve the device count
  const devices = await Device.find({ gatewayId: { $in: gatewayIds } }).sort(
    "vendor"
  );
  // create a view model for the list of gateways page
  let viewModels = [];
  // build the view model
  gateways.forEach(async (g) => {
    const gatewayId = g._id.toString();

    const viewModel = {
      id: g._id,
      serialNo: g.serialNo,
      name: g.name,
      ipAddress: g.ipAddress,
      deviceCount: devices.filter((d) => d.gatewayId == gatewayId).length,
    };

    viewModels.push(viewModel);
  });

  // return the view model
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = viewModels;

  return serviceResponse;
};

const getById = async (gatewayId) => {
  // get a gateway by its id
  const gateway = await Gateway.findById(gatewayId);

  // return failure response if gateway does not exist
  if (!gateway) {
    serviceResponse.message = "The gateway with the given ID does not exist";
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // return success response if a gateway exist
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = gateway;

  return serviceResponse;
};

const getGatewayDetailsById = async (gatewayId) => {
  // get a gateway by its id
  const gateway = await Gateway.findById(gatewayId);

  // return failure response if gateway does not exist
  if (!gateway) {
    serviceResponse.message = "The gateway with the given ID does not exist";
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // get the devices linked to this gateway to retrieve the device count
  const devices = await Device.find({ gatewayId });

  // build the view model
  const gatewayViewModel = {
    id: gateway._id,
    serialNo: gateway.serialNo,
    name: gateway.name,
    ipAddress: gateway.ipAddress,
    deviceCount: devices.length,
  };

  // return success response if a gateway exist
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = gatewayViewModel;

  return serviceResponse;
};

const add = async (model) => {
  // validate user input
  const modelState = await validate(model);
  if (!modelState.isValid) {
    serviceResponse.message = modelState.errorMessages;
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // check if the serialNo is unique
  const serialNo = await Gateway.findOne({ serialNo: model.serialNo });
  if (serialNo) {
    serviceResponse.message = `A device with Serial No [${model.serialNo}] already exist.`;
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // prepare the gateway model
  let gateway = new Gateway({
    serialNo: model.serialNo,
    name: model.name,
    ipAddress: model.ipAddress,
    dateCreated: new Date(),
  });

  // add the gateway to database
  gateway = await gateway.save();

  // return failure response if add operations fails
  if (!gateway) {
    serviceResponse.message = "Unable to save gateway information";
    serviceResponse.successful = false;
    serviceResponse.result = gateway;
    return serviceResponse;
  }

  // return success response when add operation was successful
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = gateway;

  return serviceResponse;
};

const update = async (model, gatewayId) => {
  //validate the gateway id
  if (model.id != gatewayId) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "Invalid gateway. Bad request";
    return serviceResponse;
  }

  // validate the users input
  const modelState = await validate(model);
  if (!modelState.isValid) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = modelState.errorMessages;
    return serviceResponse;
  }

  // find and update the gateway
  const gateway = await Gateway.findByIdAndUpdate(
    gatewayId,
    {
      gatewayId: model.gatewayId,
      vendor: model.vendor,
      status: model.status,
    },
    { new: true }
  );

  // return failure response if update operations fails
  if (!gateway) {
    serviceResponse.message = "The gateway with the given ID does not exist";
    serviceResponse.successful = false;
    serviceResponse.result = {};
    return serviceResponse;
  }

  // return success response if update operations fails
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = gateway;

  return serviceResponse;
};

const remove = async (gatewayId) => {
  // retrieve and remove existing gateway by the device id
  const gateway = await Gateway.findByIdAndRemove(gatewayId);

  // return failure response if devele operations fails
  if (!gateway) {
    serviceResponse.successful = false;
    serviceResponse.result = {};
    serviceResponse.message = "The gateway with the given ID does not exist";
    return serviceResponse;
  }

  // return success response if delete operation is successful
  serviceResponse.message = "";
  serviceResponse.successful = true;
  serviceResponse.result = gateway;

  return serviceResponse;
};

module.exports = {
  getAll,
  getById,
  getGatewayDetailsById,
  add,
  update,
  remove,
};
