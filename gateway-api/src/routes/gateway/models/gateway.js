const { Validator } = require("node-input-validator");
const mongoose = require("mongoose");

const Gateway = mongoose.model(
  "Gateway",
  new mongoose.Schema({
    serialNo: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    ipAddress: {
      type: String,
      required: true,
      minlength: 7,
      maxlength: 15,
    },
    dateCreated: {
      type: Date,
    },
  })
);

const validateGateway = async (gateway) => {
  const schema = {
    serialNo: "required",
    name: "required",
    ipAddress: "required|ip",
  };
  const customMessages = {
    serialNo: "Serial No is required and must be unique.",
    name: "Name is required.",
    ipAddress: "IPv4 Address is required.",
  };
  const v = new Validator(gateway, schema, customMessages);
  const isValid = await v.check();
  return { isValid, errorMessages: v.customMessages };
};

module.exports = {
  Gateway,
  validate: validateGateway,
};
