const request = require("supertest");
const { Gateway } = require("../../src/routes/gateway/models/gateway");

let server;

describe("/api/gateway", () => {
  beforeEach(() => {
    server = require("../../src/index");
  });
  afterEach(async () => {
    await server.close();
    await Genre.remove({});
  });

  describe("GET /", () => {
    it("should return all gateways", async () => {
      const gateways = [
        { serialNo: "0001", name: "Gateway 1", ipAddress: "10.0.0.1" },
        { serialNo: "0002", name: "Gateway 2", ipAddress: "10.0.0.2" },
      ];

      await Gateway.collection.insertMany(gateways);

      const res = await request(server).get("/api/gateway");

      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
    });
  });

  describe("GET /:id", () => {
    it("should return a gateway if valid id is passed", async () => {
      const gateway = new Gateway({
        serialNo: "0001",
        name: "Gateway 1",
        ipAddress: "10.0.0.1",
      });
      await gateway.save();

      const res = await request(server).get("/api/gateway/" + gateway._id);

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("name", gateway.name);
    });

    it("should return 404 if invalid id is passed", async () => {
      const res = await request(server).get("/api/gateway/1");

      expect(res.status).toBe(404);
    });

    it("should return 404 if no genre with the given id exists", async () => {
      const id = mongoose.Types.ObjectId();
      const res = await request(server).get("/api/gateway/" + id);

      expect(res.status).toBe(404);
    });
  });

  describe("POST /", () => {
    let gateway = {
      serialNo: "0001",
      name: "Gateway 1",
      ipAddress: "10.0.0.1",
    };

    const exec = async () => {
      return await request(server).post("/api/gateway").send(gateway);
    };

    it("should save the gateway if it is valid", async () => {
      await exec();

      const genre = await Gateway.find({ serialNo: "0001" });

      expect(genre).not.toBeNull();
    });

    it("should return the gateway if it is valid", async () => {
      const res = await exec();

      expect(res.body).toHaveProperty("_id");
      expect(res.body).toHaveProperty("serialNo", "0001");
    });

    it("should return 400 if the gateway ip address is invalid", async () => {
      gateway.ipAddress = "1234";
      const res = await exec();

      expect(res.status).toBe(400);
    });
  });
});
