import http from "./httpServices";
import * as config from "../../config.json";

const { Api } = config;
const gatewayUrl = Api.Gateway.baseUrl + "api/gateway";

export function getGateways() {
  return http.get(gatewayUrl);
}

export function submitGateway(gateway) {
  return http.post(gatewayUrl, gateway);
}

export function getGatewayDetailsById(gatewayId) {
  return http.get(gatewayUrl + "/getGatewayDetailsById/" + gatewayId);
}
