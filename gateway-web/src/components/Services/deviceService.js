import http from "./httpServices";
import * as config from "../../config.json";

const { Api } = config;
const deviceUrl = Api.Gateway.baseUrl + "api/device";

export function getDevices() {
  return http.get(deviceUrl);
}

export function submitDevice(device) {
  return http.post(deviceUrl, device);
}

export function getDeviceById(deviceId) {
  return http.get(deviceUrl + "/" + deviceId);
}

export function getDevicesByGatewayId(gatewayId) {
  return http.get(deviceUrl + "/getByGatewayId/" + gatewayId);
}

export function deleteDeviceById(deviceId) {
  return http.delete(deviceUrl + "/" + deviceId);
}
