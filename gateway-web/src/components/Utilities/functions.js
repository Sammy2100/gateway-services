function formatDate(date) {
  if (!date) return "";

  const newDate = new Date(date);
  const year = newDate.getFullYear();
  let month = newDate.getMonth() + 1;

  const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${newDate
    .getDate()
    .toString()
    .padStart(2, "0")}`;

  return formattedDate;
}

const CONSTANTS = {
  deviceLimitPerGateway: 10,
};

export { formatDate, CONSTANTS };
