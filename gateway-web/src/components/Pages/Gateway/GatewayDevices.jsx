import { Link } from "react-router-dom";
import { Table } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";
import { formatDate } from "../../Utilities/functions";

const GatewayDevices = ({ devices }) => {
  let i = 1;
  return (
    <Table responsive striped hover>
      <thead>
        <tr>
          <th>#</th>
          <th>UID</th>
          <th>Vendor</th>
          <th>Status</th>
          <th>Date Linked</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {devices.map((d) => (
          <tr key={d._id}>
            <th scope="row">{i++}</th>
            <td>{d.uid}</td>
            <td>{d.vendor}</td>
            <td>
              {d.status ? (
                <span className="text-success">Online</span>
              ) : (
                <span className="text-warning">Offline</span>
              )}
            </td>
            <td>{formatDate(d.dateCreated)}</td>
            <td>
              <Link
                to={{
                  pathname: ROUTEURL.GATEWAY_UNLINK,
                }}
                state={{ device: d }}
              >
                Unlink
              </Link>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default GatewayDevices;
