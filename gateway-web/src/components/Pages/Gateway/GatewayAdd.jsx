import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { Formik, Form } from "formik";
import { Col, Container, Row } from "reactstrap";
import { submitGateway } from "../../Services/gatewayService";
import TextField from "../Shared/Form/TextField";
import { ROUTEURL } from "../../Routes/routeUrl";

const GatewayAdd = () => {
  const navigate = useNavigate();
  const [error, setError] = useState("");

  useEffect(() => {
    return () => {
      // cleanup function for memory leaks
    };
  }, [error]);

  const initialValues = {
    ipAddress: "",
    serialNo: "",
    name: "",
  };

  const yupValidation = Yup.object({
    ipAddress: Yup.string()
      .matches(
        /((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}/,
        "IP Address is not in the correct format!"
      )
      .required("IP Address is required!")
      .min(7, "Invalid IP address!")
      .max(15, "Invalid IP Address!"),
    name: Yup.string().required("Name is required!"),
    serialNo: Yup.string()
      .min(1, "Must be 1 chars or more")
      .required("Serial Numner is required!"),
  });

  return (
    <Container>
      <Row>
        <Col>
          <Formik
            initialValues={initialValues}
            validationSchema={yupValidation}
            onSubmit={async (values, { setSubmitting }) => {
              setError("");
              setSubmitting(true);
              let listingDiv = "";

              try {
                const { data } = await submitGateway(values);
                if (data.successful && data.result != {}) {
                  navigate(ROUTEURL.GATEWAY_LISTING);
                } else {
                  listingDiv = (
                    <span className="text-danger">{data.message}</span>
                  );
                }
              } catch (ex) {
                listingDiv = (
                  <span className="text-danger">
                    An error occured while linking device to the gateway data.
                    Try again later.
                  </span>
                );
              }

              setError(listingDiv);
            }}
          >
            {(props) => (
              <div className="center">
                <Col className="center-text">
                  <h3 className="fw-bolder">Add a Gateway</h3>
                </Col>

                <span className="text-danger">{error}</span>
                <Form className="form-box">
                  <TextField label="Serial No" name="serialNo" type="text" />
                  <TextField label="Name" name="name" type="text" />
                  <TextField
                    label="IP Address"
                    name="ipAddress"
                    type="text"
                    pattern="^((\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.){3}(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$"
                    placeholder="xxx.xxx.xxx.xxx"
                  />
                  <button className="btn btn-dark mt-3" type="submit">
                    Add
                  </button>
                  -
                  <button className="btn btn-primary mt-3 ml-3" type="reset">
                    Reset
                  </button>
                  -
                  <button
                    onClick={() => {
                      navigate(-1);
                    }}
                    className="btn btn-danger mt-3 ml-3"
                    type="button"
                  >
                    Cancel
                  </button>
                </Form>
              </div>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};

export default GatewayAdd;
