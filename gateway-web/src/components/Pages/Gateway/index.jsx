import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Col, Container, Row, Table } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";
import { getGateways } from "../../Services/gatewayService";
import Loader from "../Shared/Loader";
import Listing from "./GatewayListing";

const Gateway = () => {
  const [loader, setLoader] = useState(true);
  const [listing, setListing] = useState("");

  useEffect(() => {
    // fetch gateways from api
    (async () => {
      let listingDiv = "";
      try {
        const { data } = await getGateways();
        if (data.successful) {
          listingDiv = <Listing gateways={data.result} />;
        } else {
          listingDiv = <span className="text-danger">{data.message}</span>;
        }
      } catch (ex) {
        listingDiv = (
          <span className="text-danger">
            An error occured while loading gateway data. Try again later.
          </span>
        );
      }
      setListing(listingDiv);
      setLoader(false);
    })();
  }, [setLoader, setListing]);
  return (
    <Container className="center">
      <Row>
        <Col className="center-text">
          <h3 className="fw-bolder ">Gateways</h3>
        </Col>
      </Row>
      <Row>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>
                <Link
                  to={{
                    pathname: ROUTEURL.GATEWAY_ADD,
                  }}
                >
                  Add a Gateway
                </Link>
              </th>
            </tr>
          </thead>
        </Table>
      </Row>
      <Row>
        <Col>
          {!loader ? (
            listing
          ) : (
            <Loader text="Fetching gateways. Please wait . . ." />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default Gateway;
