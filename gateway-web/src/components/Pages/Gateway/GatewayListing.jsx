import { Link } from "react-router-dom";
import { Table } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";
import { CONSTANTS } from "../../Utilities/functions";

const Gateway = ({ gateways }) => {
  let i = 1;

  return (
    <Table responsive striped hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Serial No</th>
          <th>Name</th>
          <th>IP Address</th>
          <th>Devices</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {gateways.map((g) => (
          <tr key={g.id}>
            <th scope="row">{i++}</th>
            <td>{g.serialNo}</td>
            <td>{g.name}</td>
            <td>{g.ipAddress}</td>
            <td>{g.deviceCount}</td>
            <td>
              <Link
                to={{
                  pathname: ROUTEURL.GATEWAY_DETAIL,
                }}
                state={{ gateway: g }}
              >
                View
              </Link>{" "}
              {g.deviceCount < CONSTANTS.deviceLimitPerGateway ? (
                <span>
                  |{" "}
                  <Link
                    to={{
                      pathname: ROUTEURL.GATEWAY_LINK,
                    }}
                    state={{ gateway: g }}
                  >
                    Link
                  </Link>
                </span>
              ) : (
                ""
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default Gateway;
