import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { Col, Container, Row, Table } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";
import { getDevicesByGatewayId } from "../../Services/deviceService";
import { CONSTANTS } from "../../Utilities/functions";
import Loader from "../Shared/Loader";
import GatewayDevices from "./GatewayDevices";

const GatewayDetail = () => {
  const [loader, setLoader] = useState(true);
  const [listing, setListing] = useState("");
  const location = useLocation();
  const { gateway } = location.state;
  useEffect(() => {
    // fetch gateway devices from api
    (async () => {
      let listingDiv = "";
      try {
        const { data } = await getDevicesByGatewayId(gateway.id);
        if (data.successful) {
          listingDiv = <GatewayDevices devices={data.result} />;
        } else {
          listingDiv = <span className="text-danger">{data.message}</span>;
        }
      } catch (ex) {
        listingDiv = (
          <span className="text-danger">
            An error occured while loading gateway data. Try again later.
          </span>
        );
      }
      setListing(listingDiv);
      setLoader(false);
    })();
  }, [setLoader, setListing, gateway]);

  return (
    <Container className="center">
      <Row>
        <Col className="center-text">
          <h3 className="fw-bolder ">
            {gateway.name} ({gateway.ipAddress}) linked devices
          </h3>
        </Col>
      </Row>
      <Row>
        <Table responsive striped hover>
          <thead>
            <tr>
              <th>
                <Link
                  to={{
                    pathname: ROUTEURL.GATEWAY_LISTING,
                  }}
                >
                  Back to Gateway List
                </Link>
              </th>

              <th className="right-text">
                {gateway.deviceCount >= CONSTANTS.deviceLimitPerGateway ? (
                  ""
                ) : (
                  <Link
                    to={{
                      pathname: ROUTEURL.GATEWAY_LINK,
                    }}
                    state={{ gateway: gateway }}
                  >
                    Link a device
                  </Link>
                )}
              </th>
            </tr>
          </thead>
        </Table>
        <Row>
          <Col>
            {!loader ? (
              listing
            ) : (
              <Loader text="Fetching linked devices. Please wait . . ." />
            )}
          </Col>
        </Row>
      </Row>
    </Container>
  );
};

export default GatewayDetail;
