import { Link } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";

const NotFound = () => {
  return (
    <Container className="center">
      <Row>
        <Col className="center-text">
          <h3 className="fw-bolder ">Page not found!</h3>
          <Row>
            <Col>
              <Link
                to={{
                  pathname: ROUTEURL.GATEWAY_LISTING,
                }}
              >
                Return to Gateway List
              </Link>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};

export default NotFound;
