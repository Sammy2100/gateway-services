import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { ROUTEURL } from "../../Routes/routeUrl";

const Home = () => {
  const navigate = useNavigate();
  useEffect(() => {
    navigate(ROUTEURL.GATEWAY_LISTING);
  }, [navigate]);

  return <div></div>;
};

export default Home;
