import { useField, ErrorMessage } from "formik";
import { Input, Row } from "reactstrap";

const ChechBoxField = ({ label, placeholder, ...rest }) => {
  const [field, meta] = useField(rest);
  const { name } = field;

  return (
    <>
      <Row className="fw-bolder left-text">
        <label htmlFor={name}>{label} </label>
      </Row>
      <Row>
        <Input
          className={`form-control shadow-none mb-3 ${
            meta.touched && meta.error && "is-invalid mb-3"
          }`}
          id={name}
          {...field}
          {...rest}
        >
          <option value={false}>Offline</option>
          <option value={true}>Online</option>
        </Input>
        <ErrorMessage component="span" className="text-danger" name={name} />
      </Row>
    </>
  );
};

export default ChechBoxField;
