import { useField, ErrorMessage } from "formik";
import { Row } from "reactstrap";

const TextField = ({ label, placeholder, ...rest }) => {
  const [field, meta] = useField(rest);
  const { name } = field;

  return (
    <>
      <Row className="fw-bolder left-text">
        <label htmlFor={name}>{label} </label>
      </Row>
      <Row>
        <input
          className={`form-control shadow-none ${
            meta.touched && meta.error && "is-invalid"
          }`}
          autoComplete="off"
          placeholder={placeholder}
          id={name}
          {...field}
          {...rest}
        />
        <ErrorMessage component="span" className="text-danger" name={name} />
      </Row>
    </>
  );
};

export default TextField;
