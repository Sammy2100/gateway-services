import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";
import { ROUTEURL } from "../../Routes/routeUrl";
import { deleteDeviceById } from "../../Services/deviceService";
import { getGatewayDetailsById } from "../../Services/gatewayService";
import Loader from "../Shared/Loader";

const UnlinkDevice = () => {
  const [error, setError] = useState("");
  const location = useLocation();
  const navigate = useNavigate();

  const { device } = location.state;

  useEffect(() => {
    (async () => {
      let listingDiv = "";
      try {
        const { data } = await deleteDeviceById(device._id);
        if (data.successful && data.result != {}) {
          const { data: datum } = await getGatewayDetailsById(device.gatewayId);
          const gateway = {
            id: datum.result.id,
            serialNo: datum.result.serialNo,
            name: datum.result.name,
            ipAddress: datum.result.ipAddress,
            deviceCount: datum.result.deviceCount,
          };
          navigate(ROUTEURL.GATEWAY_DETAIL, { state: { gateway } });
        } else {
          listingDiv = <span className="text-danger">{data.message}</span>;
        }
      } catch (ex) {
        listingDiv = (
          <span className="text-danger">
            An error occured while unlinking device to the gateway data. Try
            again later.
          </span>
        );
      }
      setError(listingDiv);

      return () => {
        // cleanup function for memory leaks
      };
    })();
  }, [device, error, setError, navigate]);

  return (
    <Container>
      <Row>
        <Col className="center-text">
          <h3 className="fw-bolder ">
            Unlinking device [{device.uid} by {device.vendor} ].{" "}
            {error == "" ? "Please wait . . ." : ""}
          </h3>
        </Col>
      </Row>
      <Row>
        <Col className="center">{error == "" ? error : <Loader />}</Col>
      </Row>
    </Container>
  );
};

export default UnlinkDevice;
