import { useLocation, useNavigate } from "react-router-dom";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { Col, Container, Row } from "reactstrap";
import TextField from "../Shared/Form/TextField";
import ChechBoxField from "../Shared/Form/ChechBoxField";
import { submitDevice } from "../../Services/deviceService";
import { useEffect, useState } from "react";
import { ROUTEURL } from "../../Routes/routeUrl";

const LinkDevice = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [error, setError] = useState("");
  const { gateway } = location.state;

  useEffect(() => {
    return () => {
      // cleanup function for memory leaks
    };
  }, [error]);

  const initialValues = {
    uid: "",
    vendor: "",
    status: false,
    gatewayId: gateway.id,
  };

  const yupValidation = Yup.object({
    uid: Yup.number().required("UID is required!"),
    vendor: Yup.string()
      .min(5, "Must be 5 chars or more")
      .required("Vendor is required!"),
    status: Yup.bool().required("Status is required!"),
  });

  return (
    <Container>
      <Row>
        <Col>
          <Formik
            initialValues={initialValues}
            validationSchema={yupValidation}
            onSubmit={async (values, { setSubmitting }) => {
              setError("");
              setSubmitting(true);
              let listingDiv = "";

              try {
                const { data } = await submitDevice(values);
                if (data.successful && data.result != {}) {
                  navigate(ROUTEURL.GATEWAY_DETAIL, { state: { gateway } });
                } else {
                  listingDiv = (
                    <span className="text-danger">{data.message}</span>
                  );
                }
              } catch (ex) {
                listingDiv = (
                  <span className="text-danger">
                    An error occured while linking device to the gateway data.
                    Try again later.
                  </span>
                );
              }

              setError(listingDiv);
            }}
          >
            {(props) => (
              <div className="center">
                <h3 className="fw-bolder">
                  Link Device to Gateway [ {gateway.name} ]
                </h3>
                <span className="text-danger">{error}</span>
                <Form className="form-box">
                  <TextField label="UID" name="uid" type="text" />
                  <TextField label="Vendor" name="vendor" type="text" />
                  <ChechBoxField label="Status" name="status" type="select" />
                  <button className="btn btn-dark mt-3" type="submit">
                    Link
                  </button>
                  -
                  <button className="btn btn-primary mt-3 ml-3" type="reset">
                    Reset
                  </button>
                  -
                  <button
                    onClick={() => {
                      navigate(-1);
                    }}
                    className="btn btn-danger mt-3 ml-3"
                    type="button"
                  >
                    Cancel
                  </button>
                </Form>
              </div>
            )}
          </Formik>
        </Col>
      </Row>
    </Container>
  );
};

export default LinkDevice;
