const ROUTEURL = {
  GATEWAY_LISTING: "/gateway",
  GATEWAY_DETAIL: "/gateway/detail",
  GATEWAY_ADD: "/gateway/add",
  GATEWAY_LINK: "/gateway/linkdevice",
  GATEWAY_UNLINK: "/gateway/unlinkdevice",
};

export { ROUTEURL };
