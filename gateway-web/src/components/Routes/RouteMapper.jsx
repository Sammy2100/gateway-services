import { BrowserRouter, Route, Routes } from "react-router-dom";

import Home from "../Pages/Home";
import NotFound from "../Pages/NotFound";
import Gateway from "../Pages/Gateway/";
import GatewayDetail from "../Pages/Gateway/GatewayDetails";
import GatewayAdd from "../Pages/Gateway/GatewayAdd";
import LinkDevice from "../Pages/Device/LinkDevice";
import UnlinkDevice from "../Pages/Device/UnlinkDevice";
import { ROUTEURL } from "./routeUrl";

const RouteMapper = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Home />} />
        <Route path={ROUTEURL.GATEWAY_LISTING} element={<Gateway />} />
        <Route path={ROUTEURL.GATEWAY_DETAIL} element={<GatewayDetail />} />
        <Route path={ROUTEURL.GATEWAY_ADD} element={<GatewayAdd />} />
        <Route path={ROUTEURL.GATEWAY_LINK} element={<LinkDevice />} />
        <Route path={ROUTEURL.GATEWAY_UNLINK} element={<UnlinkDevice />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default RouteMapper;
