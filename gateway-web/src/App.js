import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import RouteMapper from "./components/Routes/RouteMapper";

function App() {
  return <RouteMapper />;
}

export default App;
