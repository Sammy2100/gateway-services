# Gateway Services

This is a sample project that manages gateways - master devices that control multiple peripheral devices.

## Technologies used

- NodeJs - Backend Api
- ReactJs - Frontend Web UI
- MongoDb - Database

## How to set it up

- Install docker and ensure that docker services are running on the machine. Open command/terminal window and run 'docker -v' to check the version of docker installed.
- Zipped package: Unzip it to a destination folder.
- Git URL: Open a command/terminal window, 'cd' into a destination folder and run the following command to download the source code: 'git clone https://Sammy2100@bitbucket.org/Sammy2100/gateway-services.git
- Using a command/terminal window, 'cd' into the gateway-services folder where you can see the 'docker-compose.yml' file when you run a 'ls' or 'dir' command.
- Run the following command 'docker-compose up' to spin up the application. For the first run, this will take a while to automatically setup the project dependencies (Node and MongoDb) in a docker container. Once the setup is complete, you will see 'gateway-web_1 | Starting the development server...' when you monitor the logs in the console/terminal window. Kindly node that after the application have been successfully configured and running for the first time, subsequent run will take nothing more than 10 to 15 seconds to run it using the same 'docker-compose up' command.
- Open you web browser and goto http://localhost:3000. This will open the gateway UI where you can add gateways and then link devices to each gateways.

## Troubleshooting

This application has been carefully package using an automated build. If you encounter an error running the application using the 'docker-compose up', kindly check if docker is properly installed and configured on your machine. You can run the 'docker -v' command to check if docker is installed.

## Who do I talk to?

- Samuel Iyomere - towumi680@yahoo.co.uk | +2348066808808 | linkedin/in/sammy210
